$LOAD_PATH << File.dirname(__FILE__)

require 'aluno.rb'
=begin
    Classe Curso => armazena os dados do curso, aluno, disciplina
=end
class Curso
    attr_accessor :alunosDoCurso, :disciplinasOferecidas
    attr_reader :codigo #atributo apenas de leitura sendo o identificador do objeto

    def initialize(codigo, aluno, disciplina)
        @codigo = codigo
        @alunosDoCurso = [].push(aluno) # atributo armazena um objeto do tipo Aluno
        @disciplinasOferecidas = [].push(disciplina)
    end

=begin
    add:
    Funcao para adicionar os dados da array nos atributos da classe
    @param: arr - lista com dados da linha do arquivo
=end
    def add(arr)
        @disciplinasOferecidas.push(arr[1])
        @alunosDoCurso.push(Aluno.new(arr[0], arr[1], arr[3],arr[5]))
    end

=begin
    calcularCRCurso:
    Funcao que calcula o CR do curso, fazendo a media dos cr com o 
        numero de alunos matriculados independe do ano letivo.
    @param: universidae - objeto do tipo Universidade com dados já armazenados 
=end
    def calcularCRCurso(universidade)
        if universidade
            somatorioCRAluno = 0
            @alunosDoCurso.each_with_index{
                |aluno, index|
                somatorioCRAluno += aluno.calcularCR(universidade)
            }
            return (somatorioCRAluno / @alunosDoCurso.size).round(2)
        end
    end
end
