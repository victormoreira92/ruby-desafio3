$LOAD_PATH << File.dirname(__FILE__)

require 'universidade.rb'
=begin
    Classe Aluno => representa a entidade aluno armazenando as notas,disciplinas cursadas e o ano letivo 
=end
class Aluno 
    attr_accessor :notas, :ano_letivo, :disciplinasCursadas
    attr_reader :matricula # atributo apenas leitura, é o identificador de matricula

    def initialize(matricula, disciplina, nota, ano_letivo)
        @matricula = matricula
        @disciplinasCursadas = [].push(disciplina)
        @notas = [].push(nota)
        @ano_letivo = [].push(ano_letivo)
    end

=begin
    add:
    Funcao para adicionar dados nos atributos disciplinaCursadas, notas, ano_letivo
    @param: disciplina,nota,ano_letivo => dados para ser adicionado em cada atributo
=end
    def add(disciplina, nota, ano_letivo)
        @disciplinasCursadas.push(disciplina)
        @notas.push(nota)
        @ano_letivo.push(ano_letivo)
    end

=begin
    calcularCR:
    Funcao calcular Coeficiente de Rendimento(CR) do aluno
    @universidade => objeto do tipo Universidade com os dados do arquivo carregado
=end
    def calcularCR(universidade)
        if universidade
            somaCRDisciplina = 0
            somatorioCargaHoraria = []
            @disciplinasCursadas.each_with_index{ |disciplinaNome, index|
                somatorioCargaHoraria.push(universidade.disciplinasUni[disciplinaNome.to_sym].to_i)
                somaCRDisciplina += somatorioCargaHoraria.last * @notas[index].to_i
            }
            return (somaCRDisciplina / somatorioCargaHoraria.sum.to_f).round(2)
        end

        
    end
end
