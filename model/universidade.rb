$LOAD_PATH << File.dirname(__FILE__)

require 'aluno.rb'
require 'curso.rb'

=begin
    Classe Universidade => em que possui armazena os dados dos aluno, cursos e disciplina.    
=end
class Universidade
    attr_accessor :alunos, :cursos, :disciplinasUni 
    def initialize
        @alunos = [] 
        @cursos = []
        @disciplinasUni = {} 
    end

=begin
    carregarDados:
    Le os dados do arquivo e cadastrar os dados.
    @param: file - localizacao do arquivo csv
=end
    def carregarDados(file)
        file = File.open(file,"r") 
        file.readlines().each_with_index{
            |line, index| 
            if index > 0
                cadastrar(line.split(','))
            end
        }
        file.close()
    end

=begin
    cadastrar:
    Cadastrar dados e criar os objetos corretos e adicionar nas respectivas listas
    @param: arr - Array das linhas do arquivo.
=end
    def cadastrar(arr)
        #alunoNovo e cursoNovo verificadores para evitar repeticao nas listas
        alunoNovo = true 
        cursoNovo = true 
        
        # Adicao dos dados nos respecivos atributos da classe
        @disciplinasUni[arr[1].to_sym] = arr[4]
        @alunos.each{ |aluno|
            if arr[0] == aluno.matricula
                aluno.add(arr[1],arr[3],arr[5]) 
                alunoNovo = false
            end
        } 
        @cursos.each{ |curso|
            if arr[2] == curso.codigo
                curso.add(arr)
                cursoNovo = false
            end
        } 
        
        #Veiricacao se a lista possui um aluno ou curso já adicionado
        if alunoNovo
            @alunos.push(Aluno.new(arr[0], arr[1], arr[3], arr[5]))
        elsif cursoNovo
            @cursos.push(Curso.new(arr[2],Aluno.new(arr[0], arr[1], arr[3], arr[5]),arr[1]))  
        end 
    end
end


