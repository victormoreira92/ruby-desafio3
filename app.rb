$LOAD_PATH << File.dirname(__FILE__)

require 'model/universidade.rb'



universidade = Universidade.new()
universidade.carregarDados("arquivoUniversidade.csv")


#Mostrar os cr dos Alunos 
puts "----------------------------------"
puts"------- O CR dos alunos é: --------"
puts
universidade.alunos.each{
    |aluno| puts "#{aluno.matricula} - #{aluno.calcularCR(universidade)}"
}
#Mostrar os cr dos Cursos 
puts 
puts "-------------------------------------"
puts "------- Média de CR dos cursos ------"
puts
universidade.cursos.each{
    |curso| puts "#{curso.codigo} - #{curso.calcularCRCurso(universidade)}"
}
